let shopBucket = document.querySelector('#buy-bucket');
let shoppingBag = document.querySelector("#shopping-bag");
let orderPizza = document.querySelectorAll('.menu-item-button');
let ordersList = document.querySelector('#orders-list-wrapp');
let finalSum = document.querySelector('#finalSum');
let clearBagBtn = document.querySelector('#clear-bag');
let constructorList = document.querySelector('#constructor-shoppingBag-list-wrapp');
let makeAnOrderBtn = document.querySelector(`#make-order`);
let submitBtn = document.querySelector("#submit-form-btn");

orderPizza.forEach(element => {
     element.addEventListener('click', addReadyPizza);
});

clearBagBtn.addEventListener('click', clearBag)
shopBucket.addEventListener('click', function () { openOrders(shoppingBag) });

function openOrders(x) {
     let closeBtn = document.querySelector('#shopping-bag-closeBtn');
     let xDisplay = (window.getComputedStyle(x, null).getPropertyValue('display'));
     closeBtn.addEventListener('click', closeBag);

     function closeBag() {
          x.style.setProperty('display', 'none');
          closeBtn.removeEventListener('click', closeBag);
     }
     if (xDisplay === "none") {
          x.style.setProperty('display', 'flex');
          x.style.setProperty('position', "fixed");
     } else {
          x.style.setProperty('display', 'none');
     }

}
function addReadyPizza() {
     let priceTag;
     let myPizzaName;
     let deleteMyElem;
     let quantity;
     for (i = 0; i < this.parentElement.children.length; i++) {
          if (this.parentElement.children[i].classList.contains('price-tag')) {
               priceTag = this.parentElement.children[i];
          }
          if (this.parentElement.children[i].classList.contains('pizza-name')) {
               myPizzaName = document.createElement('p');
               myPizzaName.innerHTML = this.parentElement.children[i].innerHTML;
               console.log(myPizzaName)
          }
     }
     deleteMyElem = document.createElement('span');
     deleteMyElem.innerHTML = '&#10006';
     deleteMyElem.setAttribute('class', 'delete-order-position')
     deleteMyElem.addEventListener('click', deleteMyParent);
     let finalPrice = document.createElement('p');
     finalPrice.innerHTML = priceTag.innerHTML;
     finalSum.innerHTML = `${Number(finalSum.innerHTML) + Number(finalPrice.innerHTML)}`;
     let smallLi = document.createElement('li');
     quantity = document.createElement('span');
     quantity.setAttribute('class', `pizza-bag-quantity`);
     quantity.innerHTML = 1;
     smallLi.setAttribute('class', 'order-positon');
     smallLi.innerHTML = `${myPizzaName.outerHTML} кількість: ${quantity.outerHTML} ціна: <span class='price-span'>${priceTag.innerHTML}</span> ₴`;
     smallLi.appendChild(deleteMyElem);
     if (ordersList.children.length == 0) {
          ordersList.appendChild(smallLi);
     }
     else if (ordersList.children.length > 0) {
          for (i = 0; i < ordersList.children.length; i++) {
               if (ordersList.children[i] != ordersList.lastElementChild) {
                    if (ordersList.children[i].children[0].innerText == myPizzaName.innerText) {
                         ordersList.children[i].children[1].innerHTML = Number(ordersList.children[i].children[1].innerHTML) + 1;
                         ordersList.children[i].children[2].innerHTML = Number(ordersList.children[i].children[2].innerHTML) + Number(finalPrice.innerHTML);
                         break
                    }
               } else if (ordersList.children[i] == ordersList.lastElementChild) {
                    if (ordersList.children[i].children[0].innerText == myPizzaName.innerText) {
                         ordersList.children[i].children[2].innerHTML = Number(ordersList.children[i].children[2].innerHTML) + Number(finalPrice.innerHTML);
                         ordersList.children[i].children[1].innerHTML = Number(ordersList.children[i].children[1].innerHTML) + 1;
                         break
                    } else {
                         ordersList.appendChild(smallLi);
                         break
                    }
               }
          }
     }
     basketCall();
}


function deleteMyParent() {
     let priceSpan;
     console.log(this.parentElement)
     for (i = 0; i < this.parentElement.children.length; i++) {
          if (this.parentElement.children[i].classList.contains('price-span')) {
               priceSpan = this.parentElement.children[i].innerHTML;
          }
          else if (this.parentElement.children[i].classList.contains('constructor-pizzas-price')) {
               priceSpan = this.parentElement.children[i].children[0].innerHTML;
          }
     }
     finalSum.innerHTML = Number(finalSum.innerHTML) - Number(priceSpan);
     this.parentElement.remove();
}

function clearBag() {
     let constructorInsideBagElements = document.querySelectorAll('.constructor-list-basket');
     constructorInsideBagElements.forEach(element => {
          element.parentElement.remove();
          element.remove();
     })
     Array.from(ordersList.children).forEach((element) => {
          element.remove();
     })
     finalSum.innerHTML = '0';
}

makeAnOrderBtn.addEventListener(`click`, makeMyOrder)

function makeMyOrder() {
     let myForm = document.querySelector(`#form-wrapper`);
     myForm.style.display = `flex`;
     document.body.style.overflow = 'hidden';
     let closeMyFormBtn = document.querySelector('#close-my-form');
     closeMyFormBtn.addEventListener(`click`, closeMyForm);
     shoppingBag.style.setProperty('display', 'none');

     function closeMyForm() {
          document.body.style.overflow = 'auto';
          myForm.style.display = `none`;
          closeMyFormBtn.removeEventListener(`click`, closeMyForm);
     }
}


submitBtn.addEventListener('click', userInformationForm);

function userInformationForm() {
     let errorMsg = document.querySelector('#error-message');
     let userEmail = document.querySelector("#email_form").value.trim();
     let userPhone = document.querySelector("#phone").value.trim();
     let userName = document.querySelector("#fname").value.trim();

     if (userEmail.length < 5) {
          if (userEmail == '') {
               errorMsg.innerHTML = 'write your email';
               return false;
          } else {
               errorMsg.innerHTML = 'email is  not valid';
               return false;
          }
     }
     if (userPhone.length < 5) {
          if (userPhone == '') {
               errorMsg.innerHTML = 'write your phone';
               return false;
          } else {
               errorMsg.innerHTML = 'phone is  not valid';
               return false;
          }
     }
     if (userName.length < 2) {
          if (userName == '') {
               errorMsg.innerHTML = 'write your name';
               return false;
          } else {
               errorMsg.innerHTML = 'name is  not valid';
               return false;
          }
     }
     errorMsg.innerText = "";
     submitBtn.setAttribute("disabled", "disabled");
     console.log(userEmail, userPhone, userName);

     let myDataRequest = new XMLHttpRequest();
     myDataRequest.open('POST', '/AJAX/data.php');
     myDataRequest.setRequestHeader('Content-Type', 'application/text/html');
     let myData = new Object();
     myData = { name: userName, email: userEmail, phone: userPhone };
     myData = myDataRequest.resp;
     myDataRequest.onload = function () {
          if (this.status >= 200 && this.status < 400) {
               let resp = this.response;
               submitBtn.removeAttribute("disabled");
               console.log(myData);
               let myForm = document.querySelector("#myForm");
               myForm.reset();
               alert(resp);
               if (!resp){
                    console.log('sorry, we suck');
               }
               else {
                    //ok
               }
          } else {
               submitBtn.removeAttribute("disabled");
               console.error();
          }
     }
     myDataRequest.send(myData);
}

function basketCall() {
     shopBucket.classList.toggle(`forBuy`);
     setTimeout(function () {
          shopBucket.classList.toggle(`forBuy`);
     }, 300);

}