let size30btn = document.querySelector('#size30');
let size40btn = document.querySelector('#size40');
let constructorSum = document.querySelector('#construction-sum-number');
let ingredients = document.querySelectorAll('.each-ingredient-figure');
let ingredientParent = document.querySelectorAll('.ingredients');
let myPizzaConstruction = document.querySelector('#construction-pizza-blank');
let constructor = document.querySelector('#all-the-ingredients');
let Counter = 1;
let ingredientsBtn = document.querySelector('#add-construction-to-basket');
let clearBtn = document.querySelector('#clear-constructor');

ingredients.forEach(element => {
    element.addEventListener('click', hemnlo);
    function hemnlo() {
        let photoName = this.getAttribute('alt');
        let me = this.parentElement.parentElement;
        let pizzaPhoto = document.createElement('img');
        let pizzaPrice = document.createElement('p');
        let pizzaName = document.createElement('h5')
        for (i = 0; i < this.parentElement.parentElement.children.length; i++) {
            if (this.parentElement.parentElement.children[i].tagName == 'P') {
                let nameP = document.createElement('span');
                nameP.setAttribute('class', 'namedPriceInConstructor');
                nameP.innerHTML = this.parentElement.parentElement.children[i].children[0].innerHTML;
                pizzaPrice.innerHTML = `Ціна: ${nameP.outerHTML} uah`;
                constructorSum.innerHTML = Number(constructorSum.innerHTML) + Number(nameP.innerHTML);
            }
            if (this.parentElement.parentElement.children[i].tagName == 'H4') {
                let nameSpan = document.createElement('span');
                nameSpan.setAttribute('class', 'namedIngredientInConstructor');
                nameSpan.innerHTML = this.parentElement.parentElement.children[i].innerHTML;
                pizzaName.innerHTML = `Інгредієнт: ${nameSpan.outerHTML}`;
            }
        }
        pizzaPhoto.style.zIndex = Counter++;
        pizzaPhoto.setAttribute('class', 'appendedImgs');
        pizzaPhoto.setAttribute('src', `imgs/index/pizza-constructor/toPizza/${photoName}.jpg`);
        myPizzaConstruction.appendChild(pizzaPhoto);
        this.style.background = 'rgba(238, 238, 238, 0.600)';
        this.style.borderRadius = '1em';
        let plusBtn = document.createElement('span');
        plusBtn.setAttribute('class', 'closeIngredient')
        if (this.parentElement.children.length < 2) {
            plusBtn.innerHTML = `1`;
            plusBtn.style.zIndex = `999`;
            this.parentElement.append(plusBtn);
        }
        addIngredient(pizzaPrice, pizzaName, me, pizzaPhoto);
    }
    function addIngredient(price, name, parent, photo) {
        let parentCloseSpan = document.createElement('span');
        parentCloseSpan.setAttribute('class', 'close-parent-ingredient');
        parentCloseSpan.innerHTML = `&#10006`;
        parent.appendChild(parentCloseSpan);
        let oneIngredient = document.createElement('div');
        oneIngredient.setAttribute('class', 'constructor-basket-ingredient');
        oneIngredient.appendChild(name);
        oneIngredient.appendChild(price);
        constructor.appendChild(oneIngredient);
        function addAmountChange() {
            let amountChanger = document.createElement('div');
            amountChanger.setAttribute('class', 'amount-Changer-wrapper');
            let moreAmount = document.createElement('img');
            moreAmount.setAttribute('class', 'amount-plus-btn')
            moreAmount.setAttribute('src', 'imgs/index/pizza-constructor/icons/plus.png')
            let lessAmount = document.createElement('img');
            lessAmount.setAttribute('class', 'amount-minus-btn')
            lessAmount.setAttribute('src', 'imgs/index/pizza-constructor/icons/negative.png')
            let amount = document.createElement('p');
            amount.innerHTML = '1';
            amountChanger.appendChild(lessAmount);
            amountChanger.appendChild(amount);
            amountChanger.appendChild(moreAmount);
            oneIngredient.appendChild(amountChanger);
            lessAmount.addEventListener('click', function () { changeAmount(lessAmount) });
            moreAmount.addEventListener('click', function () { changeAmount(moreAmount) });
            function changeAmount(btn) {
                if (btn.classList.contains('amount-plus-btn')) {
                    if (Number(amount.innerHTML) < 3) {
                        price.children[0].innerHTML = Number(price.children[0].innerHTML) + Number(parent.children[2].children[0].innerHTML);
                        parent.children[1].children[1].innerHTML = Number(parent.children[1].children[1].innerHTML) + 1;
                        amount.innerHTML = Number(amount.innerHTML) + 1;
                        constructorSum.innerHTML = Number(constructorSum.innerHTML) + Number(parent.children[2].children[0].innerHTML);
                    } else {
                        alert('максимальна кількість інгредієнтів складає 3 одиниці');
                    }
                }
                else if (btn.classList.contains('amount-minus-btn')) {
                    if (Number(amount.innerHTML) > 1) {
                        amount.innerHTML = Number(amount.innerHTML) - 1;
                        price.children[0].innerHTML = Number(price.children[0].innerHTML) - Number(parent.children[2].children[0].innerHTML);
                        parent.children[1].children[1].innerHTML = Number(parent.children[1].children[1].innerHTML) - 1;
                        constructorSum.innerHTML = Number(constructorSum.innerHTML) - Number(parent.children[2].children[0].innerHTML);
                    }
                    else {
                        btn.parentElement.parentElement.remove();
                        btn.parentElement.remove();
                        parentCloseSpan.remove()
                        myPizzaConstruction.removeChild(photo);
                        price.children[0].innerHTML = Number(price.children[0].innerHTML) - Number(parent.children[2].children[0].innerHTML);
                        constructorSum.innerHTML = Number(constructorSum.innerHTML) - Number(parent.children[2].children[0].innerHTML);
                        parent.children[1].children[1].remove();
                        parent.children[1].children[0].removeAttribute('style');
                    }
                }
            }
            function deleteParentIngredient(parentElem) {
                parentCloseSpan.remove()
                oneIngredient.remove()
                myPizzaConstruction.removeChild(photo);
                if (Number(amount.innerHTML) > 0) {
                    for (i = 0; i < Number(amount.innerHTML); i++) {

                        constructorSum.innerHTML = Number(constructorSum.innerHTML) - Number(parent.children[2].children[0].innerHTML);
                    }
                }
                parentElem.children[1].children[1].remove();
                parentElem.children[1].children[0].removeAttribute('style');
            }
            parentCloseSpan.addEventListener('click', function () { deleteParentIngredient(parent) })
        }
        addAmountChange();
    }
})



size30btn.addEventListener('click', PizzaSizeAdd);
size40btn.addEventListener('click', PizzaSizeAdd);

function PizzaSizeAdd() {
    let sizeHeader = document.querySelector('#sizeHeader');
    console.log(sizeHeader.children[0]);
    if (sizeHeader.children[0].innerHTML == '30 см') {
        if (this.value == '150') {
            constructorSum.innerHTML = Number(constructorSum.innerHTML) + 50;
        }
        else {
            constructorSum.innerHTML = Number(constructorSum.innerHTML);
        }
    }
    else if (sizeHeader.children[0].innerHTML == '40 см') {
        if (this.value == '100') {
            constructorSum.innerHTML = Number(constructorSum.innerHTML) - 50;
        }
        else {
            constructorSum.innerHTML = Number(constructorSum.innerHTML);
        }
    }
    sizeHeader.children[0].innerHTML = `${this.name}`;
}

ingredientsBtn.addEventListener('click', addIngredientsToBasket);

function addIngredientsToBasket() {
    if (constructor.children.length == 0){ alert('Ви не додали ніяких інгредієнтів')} 
    else {
    let pizzaWrapperDiv = document.createElement(`div`);
    let constructedPizzaSize = document.querySelector('#sizeHeader');
    let constructorSumBasket = document.querySelector(`#construction-sum-number`);
    let wrapperHeader = document.createElement('h5');
    let listMain = document.createElement('ol');
    let closeSpan = document.createElement(`span`);
    closeSpan.innerHTML = '&#10006';
    closeSpan.setAttribute('class', 'delete-order-position')
    closeSpan.addEventListener('click', deleteMyParent);
    listMain.setAttribute('class', 'constructor-list-basket')
    wrapperHeader.innerHTML = `Ваша піца ${Number(constructorList.children.length) + 1} | ${constructedPizzaSize.innerHTML}`;
    pizzaWrapperDiv.appendChild(wrapperHeader);
    console.log(wrapperHeader);
    for (i = 0; i < constructor.children.length; i++) {
        let myLi = document.createElement('li');
        myLi.setAttribute('class', 'constructor-elements-basket');
        for (j = 0; j < constructor.children[i].children.length; j++) {
            if (constructor.children[i].children[j].tagName == `H5`) {
                let ingredientHeaderName = document.createElement('p');
                ingredientHeaderName.setAttribute('class', 'constructor-paragraph-basket')
                ingredientHeaderName.innerHTML = constructor.children[i].children[j].innerHTML;
                myLi.appendChild(ingredientHeaderName)
            }
            if (constructor.children[i].children[j].tagName == `P`) {
                let myPrice = document.createElement('p');
                myPrice.setAttribute('class', 'constructor-price-basket')
                myPrice.innerHTML = constructor.children[i].children[j].innerHTML;
                myLi.appendChild(myPrice)
            } if (constructor.children[i].children[j].tagName == 'DIV') {
                let childAmount = document.createElement('p');
                childAmount.setAttribute('class', 'constructor-amount-basket')
                childAmount.innerHTML = `Кількість: ${constructor.children[i].children[j].children[1].innerHTML}`;
                myLi.appendChild(childAmount)
            }
            listMain.appendChild(myLi);
        }

    }
    let thisPizzaPrice = document.createElement('p');
    let thisPizzaPriceSpan = document.createElement(`span`);
    thisPizzaPriceSpan.innerHTML = constructorSumBasket.innerHTML;
    console.log(thisPizzaPriceSpan);
    thisPizzaPrice.setAttribute('class', 'constructor-pizzas-price');
    thisPizzaPrice.innerHTML = `Ціна: ${thisPizzaPriceSpan.outerHTML} ₴`;
    pizzaWrapperDiv.appendChild(thisPizzaPrice);
    pizzaWrapperDiv.appendChild(closeSpan);
    pizzaWrapperDiv.appendChild(listMain);
    constructorList.appendChild(pizzaWrapperDiv);
    pizzaWrapperDiv.addEventListener(`click`, showMyList);
    finalSum.innerHTML = Number(finalSum.innerHTML) + Number(constructorSumBasket.innerHTML);
    clearConstructor();
    basketCall(); 
}
}

clearBtn.addEventListener('click', clearConstructor);
function clearConstructor() {
    constructorSum.innerHTML = `100`;
    let sizeHeader = document.querySelector('#sizeHeader');
    sizeHeader.children[0].innerHTML = '30 см';
    //ingredients-delete
    let myIngredients = document.querySelectorAll(`.constructor-basket-ingredient`);
    myIngredients.forEach(element => {
        element.remove();
    })

    //deleteBgOnParent
    let allParentSpans = document.querySelectorAll(`.closeIngredient`);
    allParentSpans.forEach(element => {
        let allCloseSpans = document.querySelectorAll(`.close-parent-ingredient`);
        allCloseSpans.forEach(element => {
            element.remove();
        })
        element.previousElementSibling.style.background = ``;
        element.previousElementSibling.style.borderRadius = ``;
        element.remove()
    })

    let pizzaAppendedImgs = document.querySelectorAll(`.appendedImgs`);
    pizzaAppendedImgs.forEach(element => {
        element.remove()
    })
}

function showMyList(){
    for (i=0;i<this.children.length;i++){
        if (this.children[i].classList.contains(`constructor-list-basket`)){
            this.children[i].classList.toggle(`show`);
        }
    }
}